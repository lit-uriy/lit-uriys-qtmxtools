/*
    QTMXTools is a small console program to work with TMX files
    Copyright (C) 2010  Alexander Antsev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see http://www.gnu.org/licenses/gpl.html.
*/

#include "tmxsplitter.h"

#include <QTextStream>

#include "tmxwriter.h"

TMXSplitter::TMXSplitter() : AbstractTMXReader()
{
    sameTMX = 0;
    differentTMX = 0;
}

TMXSplitter::TMXSplitter(QIODevice *sameTMXFile, QIODevice *diffTMXFile)
    : AbstractTMXReader()
{
    sameTMX = 0;
    differentTMX = 0;
    setSameTMXfile(sameTMXFile);
    setDifferentTMXfile(diffTMXFile);
}

TMXSplitter::~TMXSplitter()
{
    if (sameTMX)
        delete sameTMX;
    if (differentTMX)
        delete differentTMX;
}

void TMXSplitter::setSameTMXfile(QIODevice *sameTMXFile)
{
    if (sameTMXFile) {
        if (sameTMX)
            delete sameTMX;
        sameTMX = new TMXWriter(sameTMXFile);
        sameTMX->setOriginalLang(oLang);
        sameTMX->setTranslationLang(tLang);
        sameTMX->initFile();
    }
}

void TMXSplitter::setDifferentTMXfile(QIODevice *diffTMXFile)
{
    if (diffTMXFile) {
        if (differentTMX)
            delete differentTMX;
        differentTMX = new TMXWriter(diffTMXFile);
        differentTMX->setOriginalLang(oLang);
        differentTMX->setTranslationLang(tLang);
        differentTMX->initFile();
    }
}

void TMXSplitter::setOriginalLang(const QString& lang)
{
    AbstractTMXReader::setOriginalLang(lang);
    if (sameTMX)
        sameTMX->setOriginalLang(lang);
    if (differentTMX)
        differentTMX->setOriginalLang(lang);
}

void TMXSplitter::setTranslationLang(const QString& lang)
{
    AbstractTMXReader::setTranslationLang(lang);
    if (sameTMX)
        sameTMX->setTranslationLang(lang);
    if (differentTMX)
        differentTMX->setTranslationLang(lang);
}

void TMXSplitter::handleSegment(const QString& original,
                                const QString& translation)
{
    static int different = 0;
    static int same = 0;

    if (original == translation) {
        sameTMX->writeSegment(original, translation);
        same++;
    } else {
        differentTMX->writeSegment(original, translation);
        different++;
    }

    QTextStream out(stdout);
    out << QString("Same: %1; Different: %2; Total: %3\r")
            .arg(same)
            .arg(different)
            .arg(same + different);
}
