#ifndef TMXWRITER_H
#define TMXWRITER_H

/*
    QTMXTools is a small console program to work with TMX files
    Copyright (C) 2010  Alexander Antsev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see http://www.gnu.org/licenses/gpl.html.
*/

#include <QXmlStreamWriter>

class TMXWriter : public QXmlStreamWriter
{
public:
    TMXWriter(QIODevice *device);
    void writeSegment(const QString& original, const QString& translation);
    void initFile();
    const QString& originalLang() { return oLang; };
    const QString& translationLang() { return tLang; };
    void setOriginalLang(const QString& lang);
    void setTranslationLang(const QString& lang);
    ~TMXWriter();

private:
    QString oLang;
    QString tLang;
};

#endif // TMXWRITER_H
