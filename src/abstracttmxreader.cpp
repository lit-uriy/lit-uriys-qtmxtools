/*
    QTMXTools is a small console program to work with TMX files
    Copyright (C) 2010  Alexander Antsev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see http://www.gnu.org/licenses/gpl.html.
*/

#include "abstracttmxreader.h"

AbstractTMXReader::AbstractTMXReader()
{
    setOriginalLang("EN-US");
    setTranslationLang("RU-RU");
}

AbstractTMXReader::~AbstractTMXReader()
{
}

void AbstractTMXReader::setOriginalLang(const QString& lang)
{
    if (!lang.isEmpty() && lang != tLang)
        oLang = lang;
}

void AbstractTMXReader::setTranslationLang(const QString& lang)
{
    if (!lang.isEmpty() && lang != oLang)
        tLang = lang;
}

bool AbstractTMXReader::read(QIODevice *device)
{
    setDevice(device);

    while (!atEnd()) {
        readNext();

        if (isStartElement()) {
            if (name() == "tmx")
                readTMX();
            else
                raiseError(QObject::tr("The file is not an TMX file."));
        }
    }

    return !error();
}

void AbstractTMXReader::readTMX()
{
    Q_ASSERT(isStartElement() && name() == "tmx");

    while (!atEnd()) {
        readNext();

        if (isEndElement())
            break;

        if (isStartElement()) {
            if (name() == "header")
                readHeader();
            else if (name() == "body")
                readBody();
            else
                raiseError(QObject::tr("Unkown element '%1' at line %2 column %3")
                           .arg(name().toString())
                           .arg(lineNumber())
                           .arg(columnNumber()));
        }
    }
}

void AbstractTMXReader::readHeader()
{
    Q_ASSERT(isStartElement() && name() == "header");

    setOriginalLang(attributes().value("srclang").toString());

    while (!atEnd()) {
        readNext();

        if (isEndElement())
            break;
    }
}

void AbstractTMXReader::readBody()
{
    Q_ASSERT(isStartElement() && name() == "body");

    while (!atEnd()) {
        readNext();

        if (isEndElement())
            break;

        if (isStartElement()) {
            if (name() == "tu")
                readTu();
            else
                raiseError(QObject::tr("Unkown element '%1' at line %2 column %3")
                           .arg(name().toString())
                           .arg(lineNumber())
                           .arg(columnNumber()));
        }
    }
}

void AbstractTMXReader::readTu()
{
    QString original, translation;
    QString lang;

    //<tu>
    Q_ASSERT(isStartElement() && name() == "tu");
    while (readNext() != QXmlStreamReader::StartElement) {}

    //<tuv lang="EN-US">
    Q_ASSERT(isStartElement() && name() == "tuv");
    lang = attributes().value("lang").toString();
    while (readNext() != QXmlStreamReader::StartElement) {}

    //<seg>en</seg>
    Q_ASSERT(isStartElement() && name() == "seg");
    if (lang == oLang)
        original = readElementText();
    else
        translation = readElementText();
    while (readNext() != QXmlStreamReader::EndElement) {}

    //</tuv>
    Q_ASSERT(isEndElement() && name() == "tuv");
    while (readNext() != QXmlStreamReader::StartElement) {}

    //<tuv lang="RU-RU">
    Q_ASSERT(isStartElement() && name() == "tuv");
    lang = attributes().value("lang").toString();
    while (readNext() != QXmlStreamReader::StartElement) {}

    //<seg>ru</seg>
    Q_ASSERT(isStartElement() && name() == "seg");
    if (lang == oLang)
        original = readElementText();
    else
        translation = readElementText();
    while (readNext() != QXmlStreamReader::EndElement) {}

    //</tuv>
    Q_ASSERT(isEndElement() && name() == "tuv");
    while (readNext() != QXmlStreamReader::EndElement) {}

    //</tu>
    Q_ASSERT(isEndElement() && name() == "tu");
    readNext();

    handleSegment(original, translation);
}
