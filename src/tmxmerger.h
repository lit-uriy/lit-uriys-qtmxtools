#ifndef TMXMERGER_H
#define TMXMERGER_H

/*
    QTMXTools is a small console program to work with TMX files
    Copyright (C) 2010  Alexander Antsev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see http://www.gnu.org/licenses/gpl.html.
*/

#include <QCoreApplication>     // for Q_DECLARE_TR_FUNCTIONS
#include "tmxdiffer.h"

class QTextStream;
class TMXWriter;

class TMXMerger : public TMXDiffer
{
    Q_DECLARE_TR_FUNCTIONS(TMXMerger)
public:
    TMXMerger();
    virtual ~TMXMerger();
    bool save();
protected:
    virtual void handleSegment(const QString& original,
                               const QString& translation);
private:
    QTextStream* out;
    QTextStream* in;
};

#endif // TMXMERGER_H
