/*
    QTMXTools is a small console program to work with TMX files
    Copyright (C) 2010  Alexander Antsev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see http://www.gnu.org/licenses/gpl.html.
*/

#include <QTextStream>

#include "tmxmerger.h"

#include "tmxwriter.h"

TMXMerger::TMXMerger() : TMXDiffer()
{
    out = new QTextStream(stdout);
    in = new QTextStream(stdin, QIODevice::Unbuffered | QIODevice::ReadOnly);
}

TMXMerger::~TMXMerger()
{
    delete in;
    delete out;
}

void TMXMerger::handleSegment(const QString& original,
                              const QString& translation)
{
    if (hash->contains(original)) {
        if (hash->value(original) != translation) {
            *out << "\n" << tr("Source segment: '%1'\n").arg(original);
            *out << tr("1: '%1'\n").arg(hash->value(original));
            *out << tr("2: '%1'\n").arg(translation);
            QString s;
            do {
                *out << tr("Enter your choose: ");
                out->flush();
                s = in->readLine();
            } while (s != "1" && s != "2");
            if (s == "2") {
                hash->insert(original, translation);
            }
        }
    } else {
        hash->insert(original, translation);
    }
}

bool TMXMerger::save()
{
    TMXHash::const_iterator i = hash->constBegin();
    while (i != hash->constEnd()) {
        writer->writeSegment(i.key(), i.value());
        i++;
    }
    return true;
}
