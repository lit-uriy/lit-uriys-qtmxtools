/*
    QTMXTools is a small console program to work with TMX files
    Copyright (C) 2010  Alexander Antsev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see http://www.gnu.org/licenses/gpl.html.
*/

#include <QTextStream>
#include <QFile>
#include "qxtcommandoptions.h"

#include "errcodes.h"
#include "tmxsplitter.h"
#include "tmxmerger.h"
#include "version.h"

/*! Option groups */
#define MODE_OPTION 1

QxtCommandOptions options;

void printUsage(const QxtCommandOptions& options)
{
    QTextStream out(stdout);
    out << QString("QTMXSplitter %1").arg(VERSION) << "\n";
    out << QObject::tr("Usage:\n");
    out << "\t" << "QTMXSplitter [options] <mode> <files>" << "\n";
    out << QObject::tr("Example:\n");
    out << "\t" << "QTMXSplitter [options] --split <project_save.tmx> [same.tmx [different.tmx]]" << "\n";
    out << "\t" << "QTMXSplitter [options] --diff <project_save1.tmx> <project_save2.tmx> [output.tmx]" << "\n";
    out << "\t" << "QTMXSplitter [options] --merge <project_save1.tmx> <project_save2.tmx> [output.tmx]" << "\n";
    options.showUsage(false, out);
}

void printError(const QString &errorStr)
{
    QTextStream out(stderr);
    out << errorStr;
}

bool split()
{
    QStringList files = options.positional();
    if (files.empty()) {
        printError(QObject::tr("No input file\n"));
        printUsage(options);
        return ERR_INPUT_FILE_NOT_FOUND;
    }

    //Input file
    QString filename = files.takeFirst();
    QFile file(filename);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        printError(QObject::tr("Error: cannot read file '%1'\n").arg(filename));
        printUsage(options);
        return ERR_CANT_OPEN_INPUT_FILE;
    }

    //File with the same segments
    if (!files.isEmpty())
        filename = files.takeFirst();
    else
        filename = "same.tmx";

    QFile sameTMXFile(filename);
    if (!sameTMXFile.open(QFile::WriteOnly | QFile::Text)) {
        printError(QObject::tr("Error: cannot write to file '%1'\n")
                   .arg(filename));
        return ERR_CANT_WRITE_SAMETMX_FILE;
    }

    //File with the differents segments
    if (!files.isEmpty())
        filename = files.takeFirst();
    else
        filename = "different.tmx";

    QFile diffTMXFile(filename);
    if (!diffTMXFile.open(QFile::WriteOnly | QFile::Text)) {
        printError(QObject::tr("Error: cannot write to file '%1'\n")
                   .arg(filename));
        return ERR_CANT_WRITE_DIFFTMX_FILE;
    }

    TMXSplitter reader(&sameTMXFile, &diffTMXFile);

    //Setting segments language
    if (options.count("slang"))
        reader.setOriginalLang(options.value("slang").toString());
    if (options.count("tlang"))
        reader.setTranslationLang(options.value("tlang").toString());

    return reader.read(&file);
}

bool diff()
{
    QStringList files = options.positional();
    if (files.empty()) {
        printError(QObject::tr("No input file\n"));
        printUsage(options);
        return ERR_INPUT_FILE_NOT_FOUND;
    }

    QString filename = files.takeFirst();
    if (files.isEmpty()) {
        printError(QObject::tr("Can't diff only one file\n"));
        return ERR_TOO_FEW_ARGS;
    }

    //First input file
    QFile file(filename);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        printError(QObject::tr("Error: cannot read file '%1'\n").arg(filename));
        printUsage(options);
        return ERR_CANT_OPEN_INPUT_FILE;
    }

    //Second input file
    QFile diffTMXFile(files.takeFirst());
    if (!diffTMXFile.open(QFile::ReadOnly | QFile::Text)) {
        printError(QObject::tr("Error: cannot read file '%1'\n").arg(diffTMXFile.fileName()));
        printUsage(options);
        return ERR_CANT_OPEN_INPUT_FILE;
    }

    //Output file
    if (!files.isEmpty())
        filename = files.takeFirst();
    else
        filename = "diff.tmx";

    QFile outputFile(filename);
    if (!outputFile.open(QFile::WriteOnly | QFile::Text)) {
        printError(QObject::tr("Error: cannot write to file '%1'\n")
                   .arg(filename));
        return ERR_CANT_WRITE_DIFFTMX_FILE;
    }

    TMXDiffer differ;
    differ.setBaseFile(&file);
    differ.setOutputFile(&outputFile);

    //Setting segments language
    if (options.count("slang"))
        differ.setOriginalLang(options.value("slang").toString());
    if (options.count("tlang"))
        differ.setTranslationLang(options.value("tlang").toString());

    return differ.read(&diffTMXFile);
}

int merge()
{
    QStringList files = options.positional();
    if (files.empty()) {
        printError(QObject::tr("No input file\n"));
        printUsage(options);
        return ERR_INPUT_FILE_NOT_FOUND;
    }

    QString filename = files.takeFirst();
    if (files.isEmpty()) {
        printError(QObject::tr("Can't merge only one file\n"));
        return ERR_TOO_FEW_ARGS;
    }

    //First input file
    QFile file(filename);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        printError(QObject::tr("Error: cannot read file '%1'\n").arg(filename));
        printUsage(options);
        return ERR_CANT_OPEN_INPUT_FILE;
    }

    //Second input file
    QFile file2(files.takeFirst());
    if (!file2.open(QFile::ReadOnly | QFile::Text)) {
        printError(QObject::tr("Error: cannot read file '%1'\n").arg(file2.fileName()));
        printUsage(options);
        return ERR_CANT_OPEN_INPUT_FILE;
    }

    //Output file
    if (!files.isEmpty())
        filename = files.takeFirst();
    else
        filename = "merged.tmx";

    QFile outputFile(filename);
    if (!outputFile.open(QFile::WriteOnly | QFile::Text)) {
        printError(QObject::tr("Error: cannot write to file '%1'\n")
                   .arg(filename));
        return ERR_CANT_WRITE_DIFFTMX_FILE;
    }

    TMXMerger merger;
    merger.setBaseFile(&file);
    merger.setOutputFile(&outputFile);

    //Setting segments language
    if (options.count("slang"))
        merger.setOriginalLang(options.value("slang").toString());
    if (options.count("tlang"))
        merger.setTranslationLang(options.value("tlang").toString());

    if (!merger.read(&file2))
        return false;
    if (!merger.save())
        return false;
    return true;
}

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    //Options parsing
    options.addSection(QxtCommandOptions::tr("Modes"));;
    options.add("split", QxtCommandOptions::tr("Split TMX file"),
                QxtCommandOptions::NoValue, MODE_OPTION);
    options.alias("split", "p");
    options.add("diff", QxtCommandOptions::tr("Diff two TMX files"),
                QxtCommandOptions::NoValue, MODE_OPTION);
    options.alias("diff", "d");
    options.add("merge", QxtCommandOptions::tr("Merge two TMX files"),
                QxtCommandOptions::NoValue, MODE_OPTION);
    options.alias("merge", "m");
    options.addSection(QxtCommandOptions::tr("Language"));;
    options.add("slang", QxtCommandOptions::tr("Source language"),
                QxtCommandOptions::ValueRequired);
    options.add("tlang", QxtCommandOptions::tr("Translation language"),
                QxtCommandOptions::ValueRequired);
    options.addSection(QxtCommandOptions::tr("Misc options"));;
    options.add("help", QxtCommandOptions::tr("Show help"));
    options.alias("help", "h");
    options.parse(app.arguments());

    if (options.count("help")) {
        printUsage(options);
        return ERR_NOERROR;
    }

    if (options.count("split")) {
        return split();
    }

    if (options.count("diff")) {
        return diff();
    }

    if (options.count("merge")) {
        return merge();
    }

    printUsage(options);
    return ERR_ERROR_IN_ARGUMENTS;
}
