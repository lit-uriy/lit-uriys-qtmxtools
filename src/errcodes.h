#ifndef ERRCODES_H
#define ERRCODES_H

/*
    QTMXTools is a small console program to work with TMX files
    Copyright (C) 2010  Alexander Antsev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see http://www.gnu.org/licenses/gpl.html.
*/

#define ERR_NOERROR                     0
#define ERR_TOO_FEW_ARGS                1
#define ERR_INPUT_FILE_NOT_FOUND        2
#define ERR_CANT_OPEN_INPUT_FILE        3
#define ERR_CANT_WRITE_SAMETMX_FILE     4
#define ERR_CANT_WRITE_DIFFTMX_FILE     5
#define ERR_ERROR_IN_ARGUMENTS          6

#endif // ERRCODES_H
