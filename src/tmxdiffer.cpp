/*
    QTMXTools is a small console program to work with TMX files
    Copyright (C) 2010  Alexander Antsev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see http://www.gnu.org/licenses/gpl.html.
*/

#include "tmxdiffer.h"

#include "tmxwriter.h"

TMXDiffer::TMXDiffer() : AbstractTMXReader()
{
    hasher = new TMXHasher;
    hash = hasher->hash();
    writer = 0;
}

TMXDiffer::~TMXDiffer()
{
    if (hasher)
        delete hasher;
    if (writer)
        delete writer;
}

bool TMXDiffer::setBaseFile(QIODevice *device)
{
    return hasher->read(device);
}

void TMXDiffer::setOriginalLang(const QString& lang)
{
    AbstractTMXReader::setOriginalLang(lang);
    if (hasher)
        hasher->setOriginalLang(lang);
    if (writer)
        writer->setOriginalLang(lang);
}

void TMXDiffer::setTranslationLang(const QString& lang)
{
    AbstractTMXReader::setTranslationLang(lang);
    if (hasher)
        hasher->setTranslationLang(lang);
    if (writer)
        writer->setTranslationLang(lang);
}


void TMXDiffer::setOutputFile(QIODevice *device)
{
    if (writer)
        delete writer;
    writer = new TMXWriter(device);
    writer->setOriginalLang(oLang);
    writer->setTranslationLang(tLang);
    writer->initFile();
}

void TMXDiffer::handleSegment(const QString& original,
                              const QString& translation)
{
    if (hash->contains(original) && (hash->value(original) == translation))
            return;

    writer->writeSegment(original, translation);
}
