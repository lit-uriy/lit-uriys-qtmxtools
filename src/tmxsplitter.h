#ifndef TMXREADER_H
#define TMXREADER_H

/*
    QTMXTools is a small console program to work with TMX files
    Copyright (C) 2010  Alexander Antsev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see http://www.gnu.org/licenses/gpl.html.
*/

#include "abstracttmxreader.h"

class TMXWriter;

class TMXSplitter : public AbstractTMXReader
{
public:
    TMXSplitter();
    TMXSplitter(QIODevice *sameTMXFile, QIODevice *diffTMXFile);
    void setSameTMXfile(QIODevice *sameTMXFile);
    void setDifferentTMXfile(QIODevice *diffTMXFile);
    virtual void setOriginalLang(const QString& lang);
    virtual void setTranslationLang(const QString& lang);
    virtual ~TMXSplitter();

protected:
    TMXWriter* sameTMX;
    TMXWriter* differentTMX;

    virtual void handleSegment(const QString& original,
                               const QString& translation);
};

#endif // TMXREADER_H
