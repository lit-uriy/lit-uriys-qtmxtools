/*
    QTMXTools is a small console program to work with TMX files
    Copyright (C) 2010  Alexander Antsev

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see http://www.gnu.org/licenses/gpl.html.
*/

#include "tmxwriter.h"

#include "version.h"

TMXWriter::TMXWriter(QIODevice *device)
{
    setOriginalLang("EN-US");
    setTranslationLang("RU-RU");

    setAutoFormatting(true);
    setDevice(device);
}

TMXWriter::~TMXWriter()
{
    writeEndDocument();
}

void TMXWriter::setOriginalLang(const QString& lang)
{
    if (!lang.isEmpty() && lang != tLang)
        oLang = lang;
}

void TMXWriter::setTranslationLang(const QString& lang)
{
    if (!lang.isEmpty() && lang != oLang)
        tLang = lang;
}

void TMXWriter::initFile()
{
    writeStartDocument();
    writeDTD("<!DOCTYPE tmx SYSTEM \"tmx11.dtd\">");

    //<tmx>
    writeStartElement("tmx");
    writeAttribute("version", "1.1");

    //<header>
    writeStartElement("header");
    writeAttribute("creationtool", "QTMXSplitter");
    writeAttribute("creationtoolversion", VERSION);
    writeAttribute("segtype", "sentence");
    writeAttribute("adminlang", oLang);
    writeAttribute("srclang", oLang);
    writeEndElement();

    //<body>
    writeStartElement("body");
}

void TMXWriter::writeSegment(const QString& original, const QString& translation)
{
    writeStartElement("tu");
    writeStartElement("tuv");
    writeAttribute("lang", oLang);
    writeTextElement("seg", original);
    writeEndElement(); //</tuv>
    writeStartElement("tuv");
    writeAttribute("lang", tLang);
    writeTextElement("seg", translation);
    writeEndElement(); //</tuv>
    writeEndElement(); //</tu>
}
